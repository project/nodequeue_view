<?php
/**
 * @file
 */

/**
 * Callback function to supply a list of content types.
 */
function nq_view_nq_view_pane_ctools_content_types() {
  return array(
    'title' => t('Nodequeue view'),
    'description' => t('View the content of a nodequeue.'),
    'defaults' => array(
      'nodequeue_qid' => '',
    ),
    'icon' => 'icon_block_custom.png',
    'single' => TRUE,
    'content_types' => array('nq_view_pane'),
    'render callback' => 'nq_view_pane_render',
    'edit form' => 'nq_view_pane_edit_form',
    'category' => t('Egmont'),
    'js' => array('misc/autocomplete.js'),
  );
}

/**
 * Run-time rendering of the panels nodequeue pane.
 */
function nq_view_pane_render($subtype, $conf, $args, $context) {

  // Get qid from from string
  $preg_matches = array();
  $match = preg_match('/\[qid: (\d+)\]/', $conf['nodequeue_qid'], $preg_matches);
  if (!$match) {
    $match = preg_match('/^qid: (\d+)/', $conf['nodequeue_qid'], $preg_matches);
  }

  if ($match) {
    $qid = $preg_matches[1];
  }


  //TODO: caching?
  if (is_numeric($qid)) {
    // Load the nodequeue
    $queue = nodequeue_load($qid);

    $qids = array();
    if ($queue->subqueues == 1) {
      $subqueues = nodequeue_load_subqueues_by_queue($queue->qid);
    }

    $nodes = array();
    foreach ($subqueues as $subqueue) {
      // This doesn't take into account / handle subqueues
      $nodes = nodequeue_load_nodes($subqueue->sqid, FALSE, $conf['offset'], $conf['limit'], TRUE);
    }

  }

  if (is_array($nodes)) {

    // TODO: delta!
    $block = new stdClass();
    $block->module = 'nq_view';
    $block->delta = 0;
    $block->content = theme('nq_view_pane', $conf, $nodes);
    $block->title = '';
    // check override title
    if ($conf['override_title']) {
      $block->title = $conf['override_title_text'];
    }

    return $block;
  }
  else {
    return NULL;
  }
  
}

/**
 * 'Edit form' callback for the panels nodequeue pane type.
 */
function nq_view_pane_edit_form(&$form, &$form_state) {

  $form['nodequeue_qid'] = array(
    '#type' => 'textfield',
    '#autocomplete_path' => 'nq_view/autocomplete',
    '#default_value' => $form_state['conf']['nodequeue_qid'] ? $form_state['conf']['nodequeue_qid'] : '',
  );

  $form['display_type'] = array(
    '#type' => 'select',
    '#default_value' => $form_state['conf']['display_type'] ? $form_state['conf']['display_type'] : 'teaser',
    '#options' => array(
      'list' => 'list',
      'teaser' => 'teaser',
      'full_node' => 'full_node'
    ),
  );

  $form['offset'] = array(
    '#type' => 'textfield',
    '#default_value' => $form_state['conf']['offset'] ? $form_state['conf']['offset'] : 0,
  );

  $form['limit'] = array(
    '#type' => 'textfield',
    '#default_value' => $form_state['conf']['limit'] ? $form_state['conf']['limit'] : 10,
  );

  // TODO - save id not string
  return $form;
}

function nq_view_pane_edit_form_submit(&$form, &$form_state) {
  foreach (array('nodequeue_qid', 'display_type', 'offset', 'limit') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Callback to provide the administrative title of the nodequeue pane.
 */
function nq_view_nq_view_pane_content_type_admin_title($subtype, $conf) {
  $output = t('Nodequeue view');
  return $output;
}

/**
 * Callback to provide administrative info for the nodequeue pane.
 */
function nq_view_nq_view_pane_content_type_admin_info($subtype, $conf) {
  $block = new stdClass();
  $block->title = $conf['nodequeue_qid'];

  return $block;
}

